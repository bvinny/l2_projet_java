package ulco.cardGame.common.games.component;


import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.util.List;

public abstract class BoardGame implements Game {

    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started;

    //
    public BoardGame(String name, Integer maxPlayers, String filename) {
        this.name = name;
        this.maxPlayers = maxPlayers;
        this.initialize(filename);
    }

    protected BoardGame() {
    }

    public boolean addPlayer(Player player) {
        //verification de la capacité des joeurs
        //si le nombre maximal atteint alors la partie peut commencer
        //impossible joueur meme nom
       return  addPlayer(player);
    }
//supprime un joueur de la liste des joueurs
    public void removePlayer(Player player) {
        removePlayer(player);
    }
//supprime tous les joueurs du jeu
    public void removePlayers(){
       removePlayers();
    }
//affiche l'ensemble des joueurs
public void displayState(){

    getPlayers(players);
}
//specifie si le jeu a commencé ou non
public boolean isStarted(){
        return isStarted();
}
//retourne le nombre maximal des joueurs
    public Integer maxNumberOfPlayers(){
        return maxNumberOfPlayers();
    }
//permet de recupérer la liste des joueurs
public List<Player> getPlayers(){
 return getPlayers();
}
}