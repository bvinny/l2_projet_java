package ulco.cardGame.common.games.component;

import ulco.cardGame.common.interfaces.Player;

import java.util.List;

public abstract class CardPlayer implements Player {
    List<Card> cards;

    public CardPlayer(String name){

    }
    public Integer getScore(){

    }
    public void addComponent(Component component){

    }
    public void removeComponent(Component component){

    }
    public Card play(){

    }
    public List<Component> getComponents(){

    }
    public void shuffleHand(){

    }
    public void clearHand(){

    }
    public String toString(){

    }
}
