package ulco.cardGame.common.games.component;

import ulco.cardGame.common.interfaces.Player;

public abstract class  BoardPlayer implements Player {
    String name;
    boolean playing;
    Integer score;

    public BoardPlayer(String name){
        this.name=name;
    }

    public void canPlay(boolean playing){

    }
    public String getName(){

    }
    public boolean isPlaying(){

    }
    public String toString(){

    }
}
