package ulco.cardGame.common.games.component;

public class Card extends Component{
    private boolean hidden;

    public Card(String name, Integer value, boolean hidden) {
        super(name, value);
        this.hidden = hidden;
    }
}
