package ulco.cardGame.common.games.component;

import ulco.cardGame.common.interfaces.Player;

import java.util.List;

public abstract class CardGame extends BoardGame{
    List<Card> cards;
    Integer numberOfRounds;

    public CardGame(String name, Integer maxPlayers, String filename){
     new BoardGame(name, maxPlayers, filename) {
         @Override
         public void initialize(String filename) {

         }

         @Override
         public Player run() {
             return null;
         }

         @Override
         public List<Player> getPlayers(List<Player> players) {
             return null;
         }

         @Override
         public boolean end() {
             return false;
         }
     };
    }
    public void initialize(String filename){
        this.initialize(filename);
    }

    public Player run() {
        return null;
    }
    public boolean end(){

        return false;
    }
    public String toString(){
        return   "name='" + name ;
    }
}
