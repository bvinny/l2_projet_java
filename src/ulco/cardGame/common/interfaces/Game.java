package ulco.cardGame.common.interfaces;

import java.util.List;

public interface Game {

    /**
     * Initialize the whole game using a parameter file
     * @param filename
     */
    void initialize(String filename);

    /**
     *
     * @return
     */
    Player run();

    /**
     * Add player to the current Game
     * @param player
     */
    boolean addPlayer(Player player);

    /**
     * Remove player from the game using the reference
     * @param player
     */
    void removePlayer(Player player);

    /**
     * Remove players from the game
     */
    void removePlayers();

    /**
     * Return player from current Game
     * @return
     * @param players
     */
    List<Player> getPlayers(List<Player> players);

    /**
     * Specify if game is started or not
     * @return
     */
    boolean isStarted();

    /**
     * current number of players inside the Game
     * @return
     */
    Integer maxNumberOfPlayers();

    /**
     * Specify if the Game has end or not
     * @return
     */
    boolean end();

    /**
     * Display current Game state
     * Such as user state (name and score)
     */
    void displayState();
}
